﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_01.Controllers
{
    public class CodingidController : Controller
    {
        // GET: Codingid
       
        [Route("Codingid/CodingIDMessage")]
        [HttpGet]
        public ActionResult CodingIDMessage()
        {
            string html = "<form method = 'post' >" +
                "<input type = 'text' name = 'name' />" +
                "<input type = 'submit' value = 'Greet me' />" +
                "</form>";
            return Content(html, "text/html");

            // return View();
        }

        [Route("Codingid/CodingIDMessage")]
        [HttpPost]
        public ActionResult Display(string name = "World")
        {
            return Content(string.Format("<h1>Hello " + name + "</h1>"), "text/html");
        }
        

        [Route("Codingid/Aloha")]
        public ActionResult Goodbye()
        {
            return Content("Goodbye");
        }
    }
}